Rep�blica Bolivariana de Venezuela
Ministerio del Poder Popular Para la Educaci�n
Instituto Universitario "Jes�s Obrero" Los flores de Catia
Carrera: Inform�tica
III Semestre  
Materia: Algoritmo y Programaci�n II 

GIT
Profesor:								      Estudiante:  
Wuinder Colmenares						Galv�n, Vanessa 
C.I.: 28.310.101


�Qu� es GIT?
	Es un sistema control de versiones muy utilizado en el desarrollo de software, donde se pueden hacer muchos cambios o versiones que requiera un proyecto, hace mantenimientos de un proyecto cuando se tiene un gran n�mero de archivos y proporciona herramientas que ayudan al desarrollo de un software o alguna p�gina web en equipo de manera eficaz. 

* Commmit: Son todos los cambios que un desarrollador realiza en un repositorio para despu�s enviarlo al proyecto principal de un equipo de trabajo. 

* Repositorio: Es donde se guarda el c�digo fuente, permite tener todos los archivos de un proyecto y el historial de revisiones de cada uno de ellos.


* Ramas: En git todo se trabaja en ramas, son caminos que est�n divididos pero conectados por la rama principal. 

Importancia de GIT 
Mediante los sistemas de control de versiones se tienen que destacar los mejores desarrollos en un proyecto.

  �C�mo Subir un Archivo a Gitlab?
1. Descargar GIT y crear una cuenta.				
2. Ingresar a la consola de Git.
3. Escribir el comando: git config -global user.name "Nombre del Usuario"
4. Escribir el comando: git config -global user.email usuario@gmail.com.
5. Escribir la ruta donde se encuentra ubicado el proyecto.
6. Escribir git init.
7. Escribir el comando git remote add origin seguido de la ruta del proyecto de GitLab.
8. Utilizar el comando git status, donde est�n todos los archivos que est�n creados en un proyecto listo para ser enviado. 
9. Utilizar el comando: git commit -m "comentario" para escribir el proyecto (nombre) 
10.Al final escribir el comando : git push -u origin master donde es enviado con los cambios.

Algunos Comandos en GIT son:

>  git status: Es un comando que muestra la informaci�n del estado actual del repositorio.

> git add: es cuando hay alguna modificaci�n, eliminaci�n o creaci�n de un archivo, estos cambios solo no suceden en un siguiente commit.

> git push: env�a los commit a un repositorio remoto con los cambios realizados.


> git revert: es para deshacer cambios.

> git clone: sirve para descargar el c�digo fuente que existe en un repositorio remoto. 


> git branch: es �til para trabajar en un proyecto de forma paralela. 

> gif checkout: sirve para poder cambiar de una rama a otra y revisar archivos y commits.


